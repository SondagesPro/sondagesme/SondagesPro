$(document).on('ready pjax:scriptcomplete',function()
{
    $('footer a:first').attr("href","https://support.sondages.pro").attr("onclick","").attr("rel","external");
    $('footer .text-center').find("a").attr("href","https://www.sondages.pro");
    $('footer .text-right').find("a").first().attr("href","https://www.sondages.pro").html("Sondages Pro");
    $('#welcome-jumbotron .hidden-xs.custom').html($('#welcome-jumbotron .hidden-xs.custom').text().replace("LimeSurvey","SondagesPro"));
});

$(document).on('click','a[rel="external"]', function(event) {
    event.preventDefault();
    event.stopPropagation();
    window.open(this.href, '_blank');
});
/* select2 on all select */
$(document).on('ready pjax:scriptcomplete',function()
{
    /* Copy survey */
    $("select#copysurveylist").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    /* Permission */
    $("select#uidselect").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    $("select#ugidselect").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    /* Template selector */
    $("select#template").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    $("select#templatedir").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    $("select#vvcharset").select2({ theme:'bootstrap' });
    $("select#csvcharset").select2({ theme:'bootstrap' });
});
